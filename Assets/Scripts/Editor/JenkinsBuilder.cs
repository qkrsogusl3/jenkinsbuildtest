﻿using System;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

class JenkinsBuilder
{
    static string[] SCENES = FindEnabledEditorScenes();

    static string APP_NAME = "JenkinsBuildTest";

    private static string[] FindEnabledEditorScenes()
    {
        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled) continue;
            EditorScenes.Add(scene.path);
        }
        return EditorScenes.ToArray();
    }

    static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
        string res = BuildPipeline.BuildPlayer(scenes, target_dir, build_target, build_options);
        if (res.Length > 0)
        {
            throw new Exception("BuildPlayer failure: " + res);
        }
    }

    [MenuItem("Custom/CI/Build Android")]
    static void PerformAndroidBuild()
    {
        BuildOptions opt = BuildOptions.None;

        string Path = Directory.GetCurrentDirectory() + "\\Android\\" + PlayerSettings.bundleVersion;
        if (!Directory.Exists(Path))
            Directory.CreateDirectory(Path);

        string BUILD_TARGET_PATH = Path + string.Format("\\AndroidBuild_{0}_{1}.apk", PlayerSettings.bundleVersion, Application.version);

        Debug.Log(BUILD_TARGET_PATH);

        GenericBuild(SCENES, BUILD_TARGET_PATH, BuildTarget.Android, opt);
    }

}